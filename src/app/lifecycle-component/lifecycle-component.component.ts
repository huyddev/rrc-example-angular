import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lifecycle-component',
  templateUrl: './lifecycle-component.component.html',
  styleUrls: ['./lifecycle-component.component.css']
})
export class LifecycleComponentComponent implements OnInit {

  constructor() { 
    console.log("Constructor")
  }

  ngOnInit() {
    console.log("on init")
  }

  ngOnDestroy() {
    console.log("on destroy")
  }

}
