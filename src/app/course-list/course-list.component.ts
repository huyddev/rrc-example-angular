import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  @Input() courses: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    // setTimeout(() => {
    //   this.router.navigate(['/detail', {id: '4'}]);
    // }, 5000)
  }

  _goToDetail() {
    // this.router.navigate(['/detail', {id: 'okkkkk', name: 'adasd'} ]);
    this.router.navigateByUrl('/detail', { state: { id: 'okkkmkmkfbgk' } })
  }

}
