import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputAndEventemitterComponent } from './output-and-eventemitter.component';

describe('OutputAndEventemitterComponent', () => {
  let component: OutputAndEventemitterComponent;
  let fixture: ComponentFixture<OutputAndEventemitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputAndEventemitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputAndEventemitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
