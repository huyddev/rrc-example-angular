import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-output-and-eventemitter',
  templateUrl: './output-and-eventemitter.component.html',
  styleUrls: ['./output-and-eventemitter.component.css']
})
export class OutputAndEventemitterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  displayCounter(count) {
    console.log(count);
}

}
