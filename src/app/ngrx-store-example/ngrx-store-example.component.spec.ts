import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxStoreExampleComponent } from './ngrx-store-example.component';

describe('NgrxStoreExampleComponent', () => {
  let component: NgrxStoreExampleComponent;
  let fixture: ComponentFixture<NgrxStoreExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgrxStoreExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxStoreExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
