import { Course } from '../course';

export const ADD_COURSE = 'ADD_COURSE';
 
export function addCourse(state: any = {}, action) {
  switch (action.type) {
    case ADD_COURSE:
      return [...state, action.payload];
    default:
      return state;
  }
}