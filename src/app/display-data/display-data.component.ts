import { Component, OnInit } from '@angular/core';
import { Color } from './Color';

@Component({
  selector: 'app-display-data',
  templateUrl: './display-data.component.html',
  styleUrls: ['./display-data.component.css']
})
export class DisplayDataComponent implements OnInit {

  colors: Color[] = [];
  customColor: string;
  isEnterOrange: Boolean = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.colors.push(
        {
          id: 1,
          name: 'red',
          value: 'red'
        },
        {
          id: 2,
          name: 'green',
          value: 'green'
        },
        {
          id: 3,
          name: 'pink',
          value: 'pink'
        },
        {
          id: 4,
          name: 'blue',
          value: 'blue'
        }
      )
    })
  }

  _addColor() {
    console.log("custom-color", this.customColor);
    let newColor: Color;
    newColor = {
      id: this.colors[this.colors.length-1].id + 1,
      name: this.customColor,
      value: this.customColor.toLowerCase()
    }
    this.colors.push(newColor);

    if(this.customColor == 'orange') {
      this.isEnterOrange = true;
    } else {
      this.isEnterOrange = false;
    }
  }
}
