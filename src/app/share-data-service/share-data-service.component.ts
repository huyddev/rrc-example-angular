import { Component, OnInit } from '@angular/core';
import { GetEmployeesService } from '../services-example/get-employees.service'

@Component({
  selector: 'app-share-data-service',
  templateUrl: './share-data-service.component.html',
  styleUrls: ['./share-data-service.component.css']
})
export class ShareDataServiceComponent implements OnInit {

  constructor(private emServices: GetEmployeesService) { 
    // this.employees = this.emServices.getSharedEmployees()
  }

  ngOnInit() {
  }

}
