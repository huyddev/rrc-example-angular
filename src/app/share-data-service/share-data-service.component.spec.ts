import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareDataServiceComponent } from './share-data-service.component';

describe('ShareDataServiceComponent', () => {
  let component: ShareDataServiceComponent;
  let fixture: ComponentFixture<ShareDataServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareDataServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareDataServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
