import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-example',
  templateUrl: './pipe-example.component.html',
  styleUrls: ['./pipe-example.component.css']
})
export class PipeExampleComponent implements OnInit {

  docs: any = {
    listPipeDefault: "https://angular.io/api?type=pipe"
  };
  dataPromise: any;
  numberRound: number = 7.6685840293;

  constructor() { }

  ngOnInit() {
    this.dataPromise = new Promise((resolve) => {
      resolve("data promise use async pipe")
    });
  }

}
