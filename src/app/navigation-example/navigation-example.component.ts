import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-navigation-example',
  templateUrl: './navigation-example.component.html',
  styleUrls: ['./navigation-example.component.css']
})
export class NavigationExampleComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  userId: any = 1;
  dataShare: any = {
    userId: 1,
    userName: 'Angular',
    company: 'CMC Global'
  }

  ngOnInit() {
  }

  _goToPipeExample() {
    this.router.navigate(['/pipes'])
  }

  _navigateFromController() {
    this.router.navigate(['/get-data-navigation'], {
      state: {
        courseData: {
          id: 2,
          name: 'Angular...'
        }
      }
    })
  }
}
