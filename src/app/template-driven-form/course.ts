export class Course {
    constructor(
        public id: number,
        public name: string,
        public author: string,
        public price?: string
      ) {  }
}
