import { Component, OnInit } from '@angular/core';
import { Course }  from './course';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css']
})
export class TemplateDrivenFormComponent implements OnInit {

  prices: Array<string> = ['1000', '4000', '7000', '10000']

  constructor() { }

  ngOnInit() {
  }

  model = new Course(1, 'Angular 8', 'Ledvet', '4000');

  submitted = false;

  onSubmit(courseData) { 
    console.log("on submit", courseData)
    this.submitted = true; 
  }

  newCourse() {
    this.model = new Course(2, 'React 17', 'Ledvet', '7000');
    console.log("new course", this.model)
  }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

}
