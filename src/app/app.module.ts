import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { SimpleFormComponent } from './simple-form/simple-form.component';
import { DirectiveComponent } from './directive/directive.component';
import { UnderlineTextDirective } from './directive/underline-text.directive';
import { ChangeColorDirective } from './directive/change-color.directive';
import { LifecycleComponentComponent } from './lifecycle-component/lifecycle-component.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { DisplayDataComponent } from './display-data/display-data.component';
import { ComponentStylingComponent } from './component-styling/component-styling.component';
import { PipeExampleComponent } from './pipe-example/pipe-example.component';
import { RoundPipePipe } from './pipe-example/round-pipe.pipe';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { ReactiveFormExampleComponent } from './reactive-form-example/reactive-form-example.component';
import { SearchComponent } from './search/search.component';
import { Day2Component } from './day2/day2.component';
import { Round2Pipe } from './pipe-example/round2.pipe';
import { HandleApiRxjsComponent } from './handle-api-rxjs/handle-api-rxjs.component';
import { ServicesExampleComponent } from './services-example/services-example.component';
import { ShareDataServiceComponent } from './share-data-service/share-data-service.component';
import { NavigationExampleComponent } from './navigation-example/navigation-example.component';
import { GetDataNavigationComponent } from './get-data-navigation/get-data-navigation.component';
import { NavigationWithParamsComponent } from './navigation-with-params/navigation-with-params.component';
import { OutputAndEventemitterComponent } from './output-and-eventemitter/output-and-eventemitter.component';
import { EventemitterComponentChildComponent } from './eventemitter-component-child/eventemitter-component-child.component';
import { NgrxStoreExampleComponent } from './ngrx-store-example/ngrx-store-example.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseListComponent,
    CourseDetailComponent,
    SimpleFormComponent,
    DirectiveComponent,
    UnderlineTextDirective,
    ChangeColorDirective,
    LifecycleComponentComponent,
    ComponentInteractionComponent,
    DisplayDataComponent,
    ComponentStylingComponent,
    PipeExampleComponent,
    RoundPipePipe,
    TemplateDrivenFormComponent,
    ReactiveFormExampleComponent,
    SearchComponent,
    Day2Component,
    Round2Pipe,
    HandleApiRxjsComponent,
    ServicesExampleComponent,
    ShareDataServiceComponent,
    NavigationExampleComponent,
    GetDataNavigationComponent,
    NavigationWithParamsComponent,
    OutputAndEventemitterComponent,
    EventemitterComponentChildComponent,
    NgrxStoreExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
