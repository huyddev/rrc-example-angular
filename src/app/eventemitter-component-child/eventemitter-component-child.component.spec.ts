import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventemitterComponentChildComponent } from './eventemitter-component-child.component';

describe('EventemitterComponentChildComponent', () => {
  let component: EventemitterComponentChildComponent;
  let fixture: ComponentFixture<EventemitterComponentChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventemitterComponentChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventemitterComponentChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
