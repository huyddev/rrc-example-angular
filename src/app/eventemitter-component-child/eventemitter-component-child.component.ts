import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-eventemitter-component-child',
  templateUrl: './eventemitter-component-child.component.html',
  styleUrls: ['./eventemitter-component-child.component.css']
})
export class EventemitterComponentChildComponent {

  @Output() valueChange = new EventEmitter();

  constructor() { }

  counter = 0;
  valueChanged() { // You can give any function name
    this.counter = this.counter + 1;
    this.valueChange.emit(this.counter);
  }

}
