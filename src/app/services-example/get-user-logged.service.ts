import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetUserLoggedService {

  constructor() { }

  setUserLogged(user: any) {
    console.log("setUserLogged", user)
    return localStorage.setItem('user', JSON.stringify(user));
  }

  getUserLogged() {
    const user = localStorage.getItem('user');
    console.log("getUserLogged", JSON.parse(user))
    return user;
  }

  
}
