import { Component, OnInit } from '@angular/core';
import { GetEmployeesService } from './get-employees.service'
import { GetUserLoggedService } from './get-user-logged.service'

@Component({
  selector: 'app-services-example',
  templateUrl: './services-example.component.html',
  styleUrls: ['./services-example.component.css']
})
export class ServicesExampleComponent implements OnInit {

  constructor(private employeeServices: GetEmployeesService, private userService: GetUserLoggedService) { }

  ngOnInit() {
    this.getEmployees();
    this.getTypeEmployees();
    this.setUserLocal();
    this.getUserLogged();
  }

  getEmployees() {
    this.employeeServices.getEmployees().subscribe(
      data => {
        console.log("service success", data);
      },
      error => console.log("service error", error),
      () => console.log("serivce complete")
    )
  }

  getTypeEmployees() {
    const types = this.employeeServices.getTypeEmployees();
    console.log("getTypeEmployees", types)
  }

  setUserLocal() {
    const user = {
      id: 1,
      name: 'Hello Angular',
      class: 'Angular'
    }
    this.userService.setUserLogged(user);
  }

  getUserLogged() {
    const user = this.userService.getUserLogged();

  }

}
