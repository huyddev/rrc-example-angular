import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../handle-api-rxjs/employee'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetEmployeesService {

  employees: any;
  private emShared: any;

  constructor(private httpClient: HttpClient) { }

  getEmployees(): Observable<any> {
    console.log("service getEmployees");
    const headerAPI = {};
    return this.httpClient.get<Employee>('http://dummy.restapiexample.com/api/v1/employees', headerAPI);
  }

  getVipEmployees(): void {
    // function void không cần return 
  }

  getTypeEmployees(): any {
    return [
      {
        id: 1,
        name: 'internal'
      },
      {
        id: 2,
        name: 'fresher'
      },
      {
        id: 3,
        name: 'intership'
      }
    ]
  }

  getSharedEmployees(): Observable<any> {
    return this.emShared.asObservable();
  }

  updateSharedEmployees(data: any) {
    this.emShared.next(data);
  }
  
}
