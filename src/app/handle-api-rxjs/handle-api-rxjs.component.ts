import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee';

@Component({
  selector: 'app-handle-api-rxjs',
  templateUrl: './handle-api-rxjs.component.html',
  styleUrls: ['./handle-api-rxjs.component.css']
})
export class HandleApiRxjsComponent implements OnInit {

  employees: any

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this._doGetUserData();
  }

  _doGetUserData() {
    const headerAPI = {};
    let employees = this.httpClient.get<Employee>('http://dummy.restapiexample.com/api/v1/employees', headerAPI);
    console.log("httpClient => employees", employees) // this is observable
    employees.subscribe(
      (data: Employee) => {
        console.log("success", data)
      },
      error => {
        // error here
        console.log("error", error)
      },
      () => {
        // complete here
        console.log("complete")
      }
    )
  }

}
