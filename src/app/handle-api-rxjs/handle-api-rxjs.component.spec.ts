import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleApiRxjsComponent } from './handle-api-rxjs.component';

describe('HandleApiRxjsComponent', () => {
  let component: HandleApiRxjsComponent;
  let fixture: ComponentFixture<HandleApiRxjsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleApiRxjsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleApiRxjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
