import { Component, OnInit } from '@angular/core';
import { Course } from './course';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courses : Course[] = [];
  selectedCourse: any = {};

  constructor() { }

  ngOnInit() {
  }

  onBuyCourse(courseForm: any) {
    
    this.courses.push(courseForm.value)
    console.log("courseForm", this.courses)
  }

  _navigateToList() {
    
  }
}
