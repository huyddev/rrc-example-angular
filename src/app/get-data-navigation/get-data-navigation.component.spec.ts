import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetDataNavigationComponent } from './get-data-navigation.component';

describe('GetDataNavigationComponent', () => {
  let component: GetDataNavigationComponent;
  let fixture: ComponentFixture<GetDataNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetDataNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetDataNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
