import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-get-data-navigation',
  templateUrl: './get-data-navigation.component.html',
  styleUrls: ['./get-data-navigation.component.css']
})
export class GetDataNavigationComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, ) { }

  ngOnInit() {
    console.log("dataShare", history.state.data);
    console.log("courseData", history.state.courseData);
  }

}
