import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.css']
})
export class SimpleFormComponent implements OnInit {

  firstname: string = '';
  lastname: string = '';
  genders = [
    {
      id: 1,
      value: 'male'
    },
    {
      id: 2,
      value: 'female'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  clickMe() {
    console.log("firstname", this.firstname)
  }

}
