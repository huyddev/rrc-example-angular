import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationWithParamsComponent } from './navigation-with-params.component';

describe('NavigationWithParamsComponent', () => {
  let component: NavigationWithParamsComponent;
  let fixture: ComponentFixture<NavigationWithParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationWithParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationWithParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
