import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navigation-with-params',
  templateUrl: './navigation-with-params.component.html',
  styleUrls: ['./navigation-with-params.component.css']
})
export class NavigationWithParamsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log("id", this.route.snapshot.paramMap.get('id'))
  }

}
