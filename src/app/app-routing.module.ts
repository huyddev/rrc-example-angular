import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourseListComponent } from './course-list/course-list.component'
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { CoursesComponent } from './courses/courses.component';
import { SimpleFormComponent } from './simple-form/simple-form.component';
import { DirectiveComponent } from './directive/directive.component';
import { LifecycleComponentComponent } from './lifecycle-component/lifecycle-component.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { DisplayDataComponent } from './display-data/display-data.component';
import { ComponentStylingComponent } from './component-styling/component-styling.component';
import { PipeExampleComponent } from './pipe-example/pipe-example.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { ReactiveFormExampleComponent } from './reactive-form-example/reactive-form-example.component';
import { HandleApiRxjsComponent } from './handle-api-rxjs/handle-api-rxjs.component';
import { ServicesExampleComponent } from './services-example/services-example.component';
import { ShareDataServiceComponent } from './share-data-service/share-data-service.component';
import { NavigationExampleComponent } from './navigation-example/navigation-example.component';
import { GetDataNavigationComponent } from './get-data-navigation/get-data-navigation.component';
import { NavigationWithParamsComponent } from './navigation-with-params/navigation-with-params.component';
import { OutputAndEventemitterComponent } from './output-and-eventemitter/output-and-eventemitter.component';

const routes: Routes = [
  {
    path: '',
    // redirectTo: '/detail',
    // pathMatch: 'full',
    component: CourseListComponent
  },
  {
    path: 'simple-form',
    component: SimpleFormComponent
  },
  {
    path: 'directive',
    component: DirectiveComponent
  },
  {
    path: 'lifecycle',
    component: LifecycleComponentComponent
  },
  {
    path: 'component-interaction',
    component: ComponentInteractionComponent
  },
  {
    path: 'display-data',
    component: DisplayDataComponent
  },
  {
    path: 'component-styling',
    component: ComponentStylingComponent
  },
  {
    path: 'pipes',
    component: PipeExampleComponent
  },
  {
    path: 'template-driven-form',
    component: TemplateDrivenFormComponent
  },
  {
    path: 'reactive-form',
    component: ReactiveFormExampleComponent
  },
  {
    path: 'handle-api-rxjs',
    component: HandleApiRxjsComponent
  },
  {
    path: 'service-example',
    component: ServicesExampleComponent
  },
  {
    path: 'share-data-service',
    component: ShareDataServiceComponent
  },
  {
    path: 'routing-navigation',
    component: NavigationExampleComponent
  },
  {
    path: 'get-data-navigation',
    component: GetDataNavigationComponent
  },
  {
    path: 'navigation-with-params/:id',
    component: NavigationWithParamsComponent
  },
  {
    path: 'output-and-eventemitter',
    component: OutputAndEventemitterComponent
  },
  {
    path: '**',
    component: CourseListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
