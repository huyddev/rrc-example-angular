import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appChangeColor]'
})
export class ChangeColorDirective {

  @Input() color: string;

  constructor(private el: ElementRef) {
    
  }

  ngOnInit() {
    this._setColor();
  }

  _setColor() {
    this.el.nativeElement.style.color = this.color || 'red'
  }

}
