import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.css']
})
export class DirectiveComponent implements OnInit {

  pink: string = 'pink';
  @Input() myColor: string;

  constructor() { }

  ngOnInit() {
    console.log("myColor", this.myColor)
  }

}
