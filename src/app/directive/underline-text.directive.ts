import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appUnderlineText]'
})
export class UnderlineTextDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style["text-decoration"] = 'underline'
  }

  @HostListener('mouseenter') onMouseEnter() {
    this._underlineText('underline')
  }

  @HostListener('mouseleave') onMouseLeave() {
    this._underlineText('none')
  }

  private _underlineText(_underline: string) {
    this.el.nativeElement.style["text-decoration"] = _underline
  }

}
